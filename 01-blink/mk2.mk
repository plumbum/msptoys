#
# Makefile for msp430
#
# 'make' builds everything
# 'make clean' deletes everything except source files and Makefile
# You need to set TARGET, MCU and SOURCES for your project.
# TARGET is the name of the executable file to be produced
# $(TARGET).elf $(TARGET).hex and $(TARGET).txt nad $(TARGET).map are all generated.
# The TXT file is used for BSL loading, the ELF can be used for JTAG use
#
# Adapted by Alessandro Pasotti - 2013 - from original:
#   http://sourceforge.net/apps/mediawiki/mspgcc/index.php?title=Example:Makefile
#

# Adapt the following to your path

WD:=$(shell pwd)

# MSPGCC_ROOT=/usr/local/mspgcc
# COMPILER_PATH=$(MSPGCC_ROOT)/bin/
# INCLUDES := -I$(WD) -I$(WD)/grlib/ -I$(WD)/driverlib/MSP430FR5xx_6xx/ -I$(WD)/LcdDriver/ -I$(WD)/images/ -I$(WD)/fonts/

MSPGCC_ROOT=
COMPILER_PATH=
INCLUDES := -I$(WD) 

# Target MCU
ifndef MCU
MCU=msp430g2553
endif


######################################################################################

INOFILE := $(wildcard *.c)

TARGET	:= $(basename $(INOFILE))

# List all the source files here
# eg if you have a source file foo.c then list it here
SOURCES = $(INOFILE)
# Include are located in the Include directory
#INCLUDES = -IInclude
# Add or subtract whatever MSPGCC flags you want. There are plenty more
#######################################################################################
CFLAGS   = -mmcu=$(MCU) -g -Os -Wall -Wunused $(INCLUDES)
ASFLAGS  = -mmcu=$(MCU) -x assembler-with-cpp -Wa,-gstabs
LDFLAGS  = -mmcu=$(MCU) -Wl,-Map=$(TARGET).map
##LDFLAGS  = -mmcu=$(MCU) -Os -Wl,-gc-sections,-u,main -lm
CPPDEPFLAGS = -MMD -MP -MF .dep/$<.dep
########################################################################################


CC       = $(COMPILER_PATH)msp430-gcc
LD       = $(COMPILER_PATH)msp430-ld
AR       = $(COMPILER_PATH)msp430-ar
AS       = $(COMPILER_PATH)msp430-gcc
GASP     = $(COMPILER_PATH)msp430-gasp
GDB      = $(COMPILER_PATH)msp430-gdb
NM       = $(COMPILER_PATH)msp430-nm
OBJCOPY  = $(COMPILER_PATH)msp430-objcopy
RANLIB   = $(COMPILER_PATH)msp430-ranlib
STRIP    = $(COMPILER_PATH)msp430-strip
SIZE     = $(COMPILER_PATH)msp430-size
READELF  = $(COMPILER_PATH)msp430-readelf
MAKETXT  = $(COMPILER_PATH)srec_cat
CP       = cp -p
RM       = rm -f
MV       = mv
MSPDEBUG = $(COMPILER_PATH)mspdebug
########################################################################################
# the file which will include dependencies
DEPEND = $(SOURCES:.c=.d)
# all the object files
OBJECTS = $(SOURCES:.c=.o)

DEPFILES := $(patsubst %, .dep/%.dep, $(SOURCES))


LIB := 
#LIB := .lib/lib.a
LIBOBJS := $(foreach dir, $(WD)/grlib/ $(WD)/LcdDriver/ $(WD)/images/ $(WD)/fonts/ $(WD)/driverlib/MSP430FR5xx_6xx/, \
	$(patsubst %, .lib/%.o, $(wildcard $(addprefix $(dir)/, *.c *.cpp))))



# include dependencies
ifneq "$(MAKECMDGOALS)" "clean"
-include $(DEPFILES)
endif



all: target

target: $(TARGET).elf  $(TARGET).hex


$(TARGET).elf: $(LIB) $(OBJECTS)
	@echo "Linking $@"
	# @echo "$(CC)  $(LDFLAGS) $(OBJECTS) $(LIB) -o $@"
	$(CC)  $(LDFLAGS) $(OBJECTS) $(LIB) -o $@
	@echo
	@echo ">>>> Size of Firmware <<<<"
	$(SIZE) $(TARGET).elf
	echo

#%.o: %.c
#	mkdir -p .dep/$(dir $<)
#	@echo "Building $@ ... "
#	@echo "$(CC) $(CFLAGS) $(CPPDEPFLAGS) -o $@ $< $(LIB)"
#	$(CC) $(CFLAGS) $(CPPDEPFLAGS) -o $@ $< $(LIB)


%.hex: %.elf
	$(OBJCOPY) -O ihex $< $@

load: $(TARGET).elf
	@echo "\nUploading to board..."
	$(MSPDEBUG) rf2500 'erase' "load $(TARGET).elf" 'exit'

dbg:
	@echo $(MCU)
	@echo $(LIBOBJS)



# rule for making assembler source listing, to see the code
%.lst: %.c
	$(CC) -c $(ASFLAGS) -Wa,-anlhd $< > $@

%.lss: %.elf                                                                                                            
	@echo                                                                                                               
	@echo $(MSG_EXTENDED_LISTING) $@                                                                                    
	$(OBJDUMP) -h -S $< > $@     

# include the dependencies unless we're going to clean, then forget about them.
ifneq ($(MAKECMDGOALS), clean)
-include $(DEPEND)
endif
# dependencies file
# includes also considered, since some of these are our own
# (otherwise use -MM instead of -M)
#
# %.d: %.c
# 	echo "Generating dependencies $@ from $<"
# 	echo "$(CC) -M $(CFLAGS) $< >$@"
# 	$(CC) -M $(CFLAGS) $< >$@



$(LIB): $(LIBOBJS)
	$(AR) rcs $@ $?

.lib/%.c.o: %.c
	mkdir -p $(dir $@)
	echo "$(COMPILE.c) -o $@ $<"
	#$(CC) $(CFLAGS) -o $@ $<
	$(COMPILE.c) -o $@ $<

# .SILENT:
.PHONY:	clean

clean:
	-$(RM) $(OBJECTS)
	-$(RM) *.o *.elf *.lst
	#-$(RM) $(SOURCES:.c=.lst)
	-$(RM) $(DEPEND)
	-$(RM) -rf .lib .dep

debug:
	$(MSPDEBUG) rf2500 gdb
	cgdb  -d $(GDB) $(TARGET).elf
