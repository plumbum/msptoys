#include <msp430g2553.h>
#include <sys/types.h>

#define LED1 BIT0
#define LED2 BIT6

void delay_ms(unsigned int ms)
{
    unsigned int i;
    for (i = 0; i <= ms; i++)
        __delay_cycles(1000);
}

int main(void)
{
    // turn-off watchdog
    WDTCTL = WDTPW | WDTHOLD;
    // WDTCTL = WDT_MRST_32;

    //  setup DCO
    DCOCTL = CALDCO_1MHZ;
    BCSCTL1 = CALBC1_1MHZ;

    P1DIR |= LED1 | LED2;

    for (;;)
    {
        P1OUT ^= LED1;
        delay_ms(200);
        P1OUT ^= LED2;
        delay_ms(200);
        // delay_ms(1000);
    }

    return 0;
}
